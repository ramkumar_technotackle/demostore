import {
  StyleSheet,
  Text,
  Dimensions,
  View,
  ScrollView,
  Image,
} from 'react-native';
import React from 'react';
import {colours} from '../constants';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const ItemCard = props => {
  return (
    <ScrollView style={styles.main}>
      <View>
        <View style={styles.itemCard}>
          <View style={styles.cardImage}>
          {/* <Image source={{uri: props.imageUri }} /> */}
            <Image
              source={{uri:props.photo}}
              style={{
                height: height * 0.2,
                width: width * 0.4,
                resizeMode: 'contain',
                borderWidth: 1,
              }}
            />
          </View>
          <View style={styles.itemDetails}>
            <Text style={styles.itemBrand} numberOfLines = {2}>{props.test}</Text>
            <Text style={styles.itemModel}>{props.category}</Text>
            <Text style={styles.itemPrice}>${props.price}</Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default ItemCard;

const styles = StyleSheet.create({
  main: {
    height: height * 0.6,
    width: width,
  },
  itemCard: {
    height: height * 0.5,
    width: width * 0.85,
    alignContent: 'center',
    shadowColor: colours.Black,
    shadowOpacity: 0,
    elevation: 4,
    borderWidth: 0.1,
    alignSelf: 'center',
    borderRadius: 10,
    alignItems: 'center',
    backgroundColor: colours.white,
  },
  cardImage: {
    marginVertical: 20,
    marginTop: 35,
  },
  itemDetails: {
    height: height * 0.2,
    width: width * 0.85,
    alignItems: 'center',
  },
  itemBrand: {
    fontSize: 22,
    fontWeight: '500',
    color: colours.Black,
    marginTop: 20,
   width : width*0.78,
   textAlign:'center',
    
  },
  itemModel: {
    fontSize: 15,
    marginTop: 10,
  },
  itemPrice: {
    fontSize: 14,
    marginTop: 10,
    fontWeight: '600',
    color: colours.Blue,
  },
});
