import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  Pressable,
} from 'react-native';
import React from 'react';
import {useState} from 'react';
import { baseURL, colours,ELECTRONICS_1,JEWELERY_1, MENS_CLOTHING, WOMENS_CLOTHING} from '../constants';
import axios from 'axios';
import { useEffect } from 'react';
import {useDispatch} from 'react-redux';
import { setProducts } from '../redux/slice/user';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;



const ItemsList = props => {
  const dispatch = useDispatch();
  const [itemStyle, setIitemStyle] = useState(false);
  const[jewelery,setJewelery] = useState(false)
  const [receiveEnd,setReceiveEnd] = useState()
  console.log(receiveEnd);
 
  
 


const searchProduct =  ()=>{
  
  console.log(props.name)
  if (props.name=="jewelery"){
 setReceiveEnd(JEWELERY_1)
  }
  else if(props.name=="electronics"){
    setReceiveEnd(ELECTRONICS_1)
  }
  else if(props.name=="men's clothing"){
    setReceiveEnd(MENS_CLOTHING)
  }
  else if(props.name=="women's clothing"){
    setReceiveEnd(WOMENS_CLOTHING)
  }

}
  useEffect((()=>{
    if(receiveEnd!=null){
    setJewelery(true)
    console.log(receiveEnd)
     axios 
    .get(receiveEnd)
    .then(response=>{
      if(response.data){
        dispatch(setProducts(response.data))
        console.log('result is',response.data )
        setReceiveEnd(null)
 
      }
      else{
        alert(Response.data.message)
      }
    })
    
  
    .catch(err =>{
      console.log(err);
    });}
    else{
      console.log('receiveEnd is', receiveEnd)
    }
  }
  ),[receiveEnd])
    



  



  

  return (
    <ScrollView
      style={styles.items}
      horizontal={true}
      showsHorizontalScrollIndicator={false}
      automaticallyAdjustContentInsets={true}>
      <TouchableOpacity
        onPress={searchProduct}  style ={styles.items2}
        >
        <Text style={styles.listText}>{props.name}</Text>
      </TouchableOpacity>
      {/* <TouchableOpacity   onPress={() => {
          setIitemStyle(true);
        }}  style ={itemStyle ? styles.items1 : styles. items2 } >
        <Text style={styles.listText}>Laptops</Text>
      </TouchableOpacity> */}

      {/* <TouchableOpacity   onPress={() => {
          setIitemStyle(true);
        }}  style ={itemStyle ? styles.items1 : styles. items2 } >
        <Text style={styles.listText}>Phones</Text>
      </TouchableOpacity>
      
      <TouchableOpacity   onPress={() => {
          setIitemStyle(true);
        }}  style ={itemStyle ? styles.items1 : styles. items2 } >
        <Text style={styles.listText}>Drones</Text>
      </TouchableOpacity> */}
    </ScrollView>
  );
};

export default ItemsList;

const styles = StyleSheet.create({
  items: {
    height: height*0.05,
    width: width*0.4,
   
  },
  listText: {

    width: width * 0.35,
    height: height * 0.04,
    textAlign:'center',
  },

  items2: {
    width: width * 0.35,
    height: height * 0.04,
    backgroundColor :'#ffffff',
    textAlign:'center',
  },
  list: {
    height: height * 0.05,
    borderWidth: 1,
    color: 'blue',
  },
});
