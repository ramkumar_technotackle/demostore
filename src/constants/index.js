export const colours = {
    white : '#ffffff',
    Black : '#000000',
Blue: '#5956e9',
blurBlue:'#6350FF',
Border : '#C9C9C9',
borderDark : '#8c8c8c',
Mail  :'#868686',

}

export const baseURL = 'https://fakestoreapi.com';
export const productURL = 'https://fakestoreapi.com/products';
export const category = "https://fakestoreapi.com/products/categories";
export const JEWELERY_1 = "https://fakestoreapi.com/products/category/jewelery";
export const ELECTRONICS_1 = "https://fakestoreapi.com/products/category/electronics";
export const MENS_CLOTHING = "https://fakestoreapi.com/products/category/men's clothing";
export const WOMENS_CLOTHING = "https://fakestoreapi.com/products/category/women's clothing";