import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import { Provider,useDispatch } from 'react-redux';
import store from '../redux/store';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import SplashScreen from '../screens/SplashScreen';
import WelcomeScreen from '../screens/WelcomeScreen';
import LoginScreen from '../screens/LoginScreen';
import HomeScreen from '../screens/HomeScreen';
import JestaScreen from '../screens/JestaScreen';
import TabNavigator from './TabNavigator';




const Navigator = () => {
  const dispatch = useDispatch();

  const Stack =createNativeStackNavigator()
  return (
   
        <Provider store = {store}>
         <NavigationContainer independent={true}>
         <Stack.Navigator screenOptions={{headerShown: false}}>

         <Stack.Screen name = "JestaScreen" component = {JestaScreen}/>
         <Stack.Screen name = "WelcomeScreen" component = {WelcomeScreen}/>
         <Stack.Screen name = "SplashScreen" component = {SplashScreen}/>
         <Stack.Screen name = "LoginScreen" component = {LoginScreen}/>
         <Stack.Screen options={{headerShown:false}} name ="TabNavigator" component = {TabNavigator}/>
         </Stack.Navigator>
         </NavigationContainer>
         </Provider>


  )
}

export default Navigator

const styles = StyleSheet.create({})