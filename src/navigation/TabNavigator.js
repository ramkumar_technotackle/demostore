import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import HomeScreen from '../screens/HomeScreen'
import TestScreen from '../screens/TestScreen'
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeStackScreen from './bottomtabnavigator/HomeStackScreen' 
import TestStackScreen from './bottomtabnavigator/TestStackScreen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Cart from 'react-native-vector-icons/AntDesign';


const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  return (
   <Tab.Navigator  screenOptions={{headerShown: false}} >
     <Tab.Screen name="HomeScreen" component={HomeScreen} options={{tabBarLabel: 'Home',
    tabBarIcon:({color,size})=>(<MaterialCommunityIcons name="home" color={color} size={size} />
    ),}} />
     <Tab.Screen name="TestScreen" component = {TestScreen}   options={{tabBarLabel: 'Store',
    tabBarIcon:({color,size})=>(<Cart name="shoppingcart" color={color} size={size} />
    ),}} />
   </Tab.Navigator>
  )
}

export default TabNavigator

const styles = StyleSheet.create({})