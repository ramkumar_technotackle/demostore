import { createSlice } from "@reduxjs/toolkit";


const initialState={
    products : "",
    productsCategory : "",
}



const userSlicer = createSlice({
    name : "useReducer",
    initialState,
    reducers : {
        setProducts : (state,action) => ({...state,products : action.payload}),
        setProductsCategory : (state,action) => ({...state,productsCategory : action.payload}),
    },
});


export default userSlicer.reducer;

export const {setProducts,setProductsCategory}  = userSlicer.actions