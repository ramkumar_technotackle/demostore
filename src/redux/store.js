import {configureStore} from '@reduxjs/toolkit';
import {setupListeners}  from '@reduxjs/toolkit/query';
import userReducer from  './slice/user'

const store = configureStore({
    reducer : { 
        user : userReducer,
    },

    middleWare : getDefaultMiddleware =>
    getDefaultMiddleware({
        immutableCheck : false,
        serilizableCheck : false,
    }),
});
setupListeners(store.dispatch);
export default store