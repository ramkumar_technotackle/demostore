import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ScrollView,
  TextInput,
  TouchableOpacity,
  FlatList,
  image,
  Button
} from 'react-native';
import React from 'react';
import {colours, products, productURL} from '../constants';
import Search from 'react-native-vector-icons/AntDesign';
import Menu from 'react-native-vector-icons/Ionicons';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import ItemsList from '../components/ItemsList';
import ItemCard from '../components/ItemCard';
import {getActiveElement} from 'formik';
import {useEffect, useState} from 'react';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import {setProducts, setProductsCategory} from '../redux/slice/user';
import store from '../redux/store';
import { useNavigation } from '@react-navigation/native';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const HomeScreen = props => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const products = useSelector(state => state.user.products);
  const category = useSelector(state => state.user.productsCategory);
  const [goods, setGoods] = useState([]);


  console.log('values', products);
 
  {
    /* <FlatList
data={productsCategory}
renderItem={({item}) => (
 
  )}
  keyExtractor={item => item.name} horizontal={true}/> */
  }

  return (
    <ScrollView nestedScrollEnabled={true}>
      <View style={styles.main}>
        <View style={styles.search}>
          <View style={styles.options}>
            <TouchableOpacity>
              <Menu name="menu" size={25} color={colours.Black} />
            </TouchableOpacity>
          </View>

          <View style={styles.searchBar}>
            <TouchableOpacity onPress={()=>{navigation.navigate('TestScreen')}} style={styles.searchText}>
              <Search name="search1" style={styles.searchIcon} size={20} />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.header}>
          <Text style={styles.headerText}>Order online collect in store</Text>
        </View>

        <FlatList
          key="_"
          data={category}
          renderItem={({item}) => <ItemsList name={item} />}
          keyExtractor={item => '_' + item}
          horizontal={true}
        />

        <FlatList
          key="&"
          data={products}
          renderItem={({item}) => (
            <ItemCard
              test={item.title}
              photo={item.image}
              category={item.category}
              price={item.price}
            />
          )}
          keyExtractor={item => '&' + item.id}
          horizontal={true}
        />

        {/* <ItemCard />  */}
        <Button
        title="See More ->"
        onPress={() => navigation.navigate('TestScreen')}
      />
      </View>
    </ScrollView>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  main: {
    height: height,
    width: width,
  },
  search: {
    height: height * 0.1,
    width: width * 0.9,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  options: {
    height: height * 0.05,
    width: width * 0.1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchBar: {
    height: height * 0.05,
    width: width * 0.7,
   
  },
  searchText: {
    height: height * 0.045,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: colours.borderDark,
    paddingLeft: 10,
    justifyContent:'center',
  },
  header: {
    height: height * 0.2,
    width: width * 0.9,
    alignSelf: 'center',
    marginTop: 10,
  },
  headerText: {
    color: colours.Black,
    fontSize: 43,
    fontWeight: '700',
  },
 
});
