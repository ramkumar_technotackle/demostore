import React, {useState, useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Dimensions,
  Image,
  TouchableOpacity,
  Button,
  FlatList,
  Animated,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {colours} from '../constants';
import Nurse from '../assets/Images/Nurse.png';
import Carousel from 'react-native-reanimated-carousel';
import CarouselImage from '../carouselarray/CarouselImage';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import Onboarding from 'react-native-onboarding-swiper';
import OnBoardingItem from '../components/OnBoardingItem';
import Paginator from '../components/Paginator';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

// const title1 = () => {
//   return (
//     <View
//       style={{
//         height: height * 0.5,
//         width: width,
//         backgroundColor: colours.DearkBlue,
//         borderWidth: 1,
//       }}>
//       <Text>hgjh</Text>
//     </View>
//   );
// };

const SecondScreen = props => {
  const navigation = useNavigation();
  const [currentIndex, setCurrentIndex] = useState(0);
  const scrollX = useRef(new Animated.Value(0)).current;
  const slidesRef = useRef(null);
  const viewableItemsChanged = useRef(({viewableItems}) => {
    setCurrentIndex(viewableItems[0].index);
  }).current;

  const scrollTo = () => {
    if (currentIndex < CarouselImage.length - 1) {
      slidesRef.current.scrollToIndex({index: currentIndex + 1});
    } else {
      console.log('this is last slide');
    }
  };

  // const viewConfig = useRef({viewAreaCoveragePercentThreshold:50}).current

  return (
    <SafeAreaView style={styles.main}>
      <View style={{flex: 3}}>
        <FlatList
          data={CarouselImage}
          renderItem={({item}) => (
            <OnBoardingItem image={item.image} title={item.title} />
          )}
          horizontal
          showsHorizontalScrollIndicator={false}
          pagingEnabled={true}
          bounces={false}
          keyExtractor={item => item.id}
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {x: scrollX}}}],
            {useNativeDriver: false},
          )}
          scrollEventThrottle={32}
          onViewableItemsChanged={viewableItemsChanged}
          // viewabilityConfig = {viewConfig}
          ref={slidesRef}
        />
      </View>
      <View style={{height: height * 0.05}}>
        <Paginator data={CarouselImage} scrollX={scrollX} />
      </View>
      <TouchableOpacity
        style={{
          backgroundColor: colours.DearkBlue,
          width: width * 0.8,
          marginBottom: 20,
          borderRadius: 20,
          padding: 5,
        }} onPress={scrollTo} >
        <Text style={{color: colours.White, fontSize: 25, textAlign: 'center'}}>
          Next
        </Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};
export default SecondScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: colours.White,
    borderWidth: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcome: {
    height: height * 0.25,
    width: width * 0.8,
    justifyContent: 'space-around',
    marginTop: 30,
  },
  welcomeText: {
    color: colours.DearkBlue,
    fontWeight: '700',
    fontSize: 40,
    textAlign: 'center',
  },
  welcomeText2: {
    color: colours.Black,
    fontSize: 17,
    textAlign: 'center',
    paddingLeft: 5,
    paddingRight: 5,
  },
  doctorImage: {
    height: height * 0.5,
    width: width * 0.8,
  },
  docsImage: {
    flex: 1,
    alignSelf: 'center',
  },
});



// <------------------Onboarding-------------->

import React from 'react';
import {
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  Dimensions,
  Image,
} from 'react-native';
import { colours } from '../constants';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const OnBoardingItem = props => {
  return (
    <SafeAreaView styles={styles.main}>
      <View
        style={{
          flex:1,
          width: width,
          alignItems: 'center',
          borderWidth: 0,
          justifyContent: 'center',
        }}>
        <Image
          source={props.image}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 0,
          }}
        />
        <View style={{flex: 0}}>
          {/* <Text>{props.title}</Text>
          <Text>{props.title}</Text> */}
        </View>
        <View
          style={{
            height: height * 0.3,
            borderTopRightRadius: 25,
            borderTopLeftRadius: 25,
            width: width,
            borderWidth: 0,
            shadowColor:colours.DearkBlue,
          shadowOffset: {
	                  width: 0,
	                  height: 3,
                          },
            shadowOpacity: 0.29,
            shadowRadius: 4.65,
              elevation:3,
              alignItems:'center',
              justifyContent:'center'
            
          }}><Text style={{  color: colours.DearkBlue,
            fontWeight: '700',
             fontSize:40,
             textAlign:'center',}} >{props.title}</Text></View>
      </View>
    </SafeAreaView>
  );
};
export default OnBoardingItem;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#0000',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0,
  },
});



// <--------------------------Paginator----------------->

import React from 'react';
import {
  View,
  SafeAreaView,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  Animated
} from 'react-native';
import { color } from 'react-native-reanimated';
import { colours } from '../constants';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const Paginator = ({data,scrollX}) => {
  return (
    <SafeAreaView>
        <View style={{flexDirection:'row',height:64}} >
        {data.map((_,i)=>{
            const inputRange = [(i-1)*width,i*width,(i+1)*width]
            const dotWidth = scrollX.interpolate({
                inputRange,
                outputRange :[10,20,10],
                extrapolate :'clamp'
            })

            const opacity =scrollX.interpolate({
                inputRange,
                // outputRange :[0.3,1,.3],
                outputRange :[10,20,10],
                extrapolate :'clamp',
            });

            return <Animated.View  style={[styles.dot,{width:dotWidth}]} key={i.toString()} />;
        })}
        </View>

     
    </SafeAreaView>
  );
};
export default Paginator;

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#0000',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    
  },
  dot:{
    height :5,
    borderRadius:5,
    backgroundColor:colours.Black,
    width:10,
    marginRight:10,
  }
});



