import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React, {Fragment,useRef} from 'react';
import {useNavigation} from '@react-navigation/native';
import Mail from 'react-native-vector-icons/MaterialCommunityIcons'
import Lock from 'react-native-vector-icons/MaterialCommunityIcons'
import {colours,productURL,category} from '../constants';
import {Formik} from 'formik';
import * as yup from 'yup';
import axios from 'axios';
import {useDispatch} from 'react-redux';
import { setProductsCategory,setProducts } from '../redux/slice/user';
import TabNavigator from '../navigation/TabNavigator';



const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const LoginScreen = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const formRef = useRef();

  const handleSubmit = () => {
    navigation.navigate('TabNavigator')
    if (formRef.current) formRef.current.handleSubmit();
  };

  const validationSchema = yup.object().shape({
    email: yup.string()
    .email("Please enter valid email")
    .required('email is required'),

    password: yup
      .string()
      .min(8, ({min}) => `password must be atleast ${min} characters`)
      .required('password is required'),
  });

 
  const onSubmit =  (values) => {
   getData()
   getcategory() 
  

  };



  const getData = async () => {
    await  axios 
    .get( productURL)
    .then(response=>{
      if(response.data){
        dispatch(setProducts(response.data))
      //  navigation.navigate('HomeScreen');
      }
      else{
        alert(Response.data.message)
      }
    })
  
    .catch(err =>{
      console.log(err);
    });

  }

  const getcategory = async () => {
    await  axios 
    .get(category)
    .then(response=>{
      if(response.data){
        dispatch(setProductsCategory(response.data))
        console.log ('jdcsh',response.data)
      }
      else{
        alert(Response.data.message)
      }
    })
  
    .catch(err =>{
      console.log(err);
    });

  }



  return (
    <ScrollView>
    <View>
      <View style={styles.main}>
        <View style={styles.header}>
          <View style={styles.eclipse1}>
            <Image
              source={require('../assets/images/GroupEllipse.png')}
              style={styles.splashImage}
            />
          </View>
          <Text style={styles.headerText}>Welcome back</Text>
        </View>
        <View style={styles.loginBlock}>
          <View style={styles.loginBlockIn}>
            <View style={styles.loginText}>
              <Text style={styles.loginText1}>Login</Text>
            </View>

            <Formik
                innerRef={formRef}
              initialValues={{email:'', password:''}}
              onSubmit={onSubmit}    
              validationSchema={validationSchema}>
              {formikProps => (
                <Fragment>
                  <View style={styles.form}>
                    <View style={styles.formText}>
                        <Mail name="email-outline" size={20} style={styles.icon}/>
                      <Text style={{marginTop: 5}} >Email</Text>
                    </View>
                    <TextInput
                      name="email"
                      onChangeText={formikProps.handleChange('email')}
                      onBlur={formikProps.handleBlur('email')}
                      value={formikProps.values.email}
                      keyboardType="email-address"
                      style={styles.textInput}></TextInput>

                    {formikProps.errors.email && 
                    formikProps.touched.email ? (
                      <Text style={{fontSize: 10, color: 'red',textAlign:'right'}}>
                        {formikProps.errors.email}
                      </Text>
                    ) : null}

                    <View style={{...styles.formText,marginTop:20}}>
                    <Lock name="lock-outline" style={styles.icon} size={20}/>
                      <Text style={{marginTop: 5}} >Password</Text>
                    </View>

                    <TextInput
                    secureTextEntry={true}
                          name="password"
                      onChangeText={formikProps.handleChange('password')}
                      onBlur={formikProps.handleBlur('password')}
                      value={formikProps.values.password}
                      style={styles.textInput}></TextInput>

                    {formikProps.errors.password &&
                    formikProps.touched.password ? (
                      <Text style={{fontSize: 10, color: 'red',textAlign:'right'}}>
                        {formikProps.errors.password}
                      </Text>
                    ) : null}
                    <TouchableOpacity style={styles.forgotPass}>
                      <Text style={styles.forgotText}>Forgot passcode ?</Text>
                    </TouchableOpacity>
                    
                      <TouchableOpacity  style={styles.button1}
                        onPress={() => {
                          handleSubmit();
                        }}>
                        <Text style={{color: '#ffffff', fontSize: 20}}>
                          Login
                        </Text>
                      </TouchableOpacity>
                  
                    <TouchableOpacity
                      style={{...styles.forgotPass, width: width * 0.8}}>
                      <Text style={styles.forgotText}>Create account</Text>
                    </TouchableOpacity>
                  </View>
                </Fragment>
              )}
            </Formik>
          </View>
        </View>
      </View>
    </View>
    </ScrollView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  main: {
    height: height,
    width: width,
    backgroundColor: colours.Blue,
  },

  header: {
    height: height * 0.35,
    width: width * 0.75,
    alignSelf: 'center',
    position: 'relative',
  },
  headerText: {
    color: colours.white,
    fontSize: 55,
    fontWeight: '800',
    position: 'absolute',
    top: height * 0.09,
  },
  loginBlock: {
    height: height * 0.7,
    width: width,
    backgroundColor: colours.white,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginBlockIn: {
    height: height * 0.6,
    width: width * 0.85,
    backgroundColor: colours.white,
  },

  loginText: {
    height: 40,
    width: width * 0.25,
   
  },
  loginText1: {
    fontSize: 20,
    color: colours.Black,
    fontWeight: 'bold',
  },
  icon:{
    marginRight:5,
    
    marginTop:8,
  },
  form: {
    alignSelf: 'center',
    marginTop: 40,
  },
  formText: {
    width: width * 0.6,
    marginTop: 5,
    flexDirection:'row',
  },
  button1: {
    padding: 10,
    width: width * 0.8,
    alignItems: 'center',
    backgroundColor: colours.Blue,
    borderRadius: 7,
    marginTop: 50,
  },
  textInput: {
    borderBottomColor: colours.Border,
    borderBottomWidth:1
  },
  forgotPass: {
    marginTop: 10,
    width: width * 0.37,
  },
  forgotText: {
    color: colours.Blue,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
