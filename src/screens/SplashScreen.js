import {StyleSheet, Text, View, Dimensions,Image} from 'react-native';
import React from 'react';
import  {colours} from '../constants/index'
import {useNavigation} from '@react-navigation/native';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const SplashScreen = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.main}>
      <View style={styles.header}>
        <Text style={styles.headerText}>TECHIE STORE</Text>
      </View>
      <View style={styles.splashPhoto}>
        <Image
          source={require('../assets/images/splashImage(2).png')}
          style={styles.splashImage}
        />
      </View>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  main: {
    height: height,
    width: width,
    backgroundColor: colours.Blue,
    alignItems: 'center',
  },
  header: {
    height: height * 0.2,
    width: width ,
    alignSelf: 'center',
    alignItems:'center',
    justifyContent:'center',
    marginTop: 20,
  },
  headerText: {
    color: colours.white,
    fontSize: 45,
    fontWeight: '800',
    textAlign:'center',
    elevation:4,
  },
  splashPhoto: {
    height: height * 0.6,
    width: width * 0.85,
    alignSelf: 'center',
  },
  splashImage: {
    width: 400,
    height: 500,
    alignSelf: 'center',
  },
  emptyView: {
    height: 50,
    width: width,
    backgroundColor: colours.blurBlue,
  },
});
