import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ScrollView,
  TextInput,
  TouchableOpacity,
  FlatList,
  image,
  Button,
  ActivityIndicator
} from 'react-native';
import React from 'react';
import {colours, jewelery1, products, productURL} from '../constants';
import Search from 'react-native-vector-icons/AntDesign';
import Menu from 'react-native-vector-icons/Ionicons';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import ItemsList from '../components/ItemsList';
import ItemCard from '../components/ItemCard';
import {getActiveElement} from 'formik';
import {useEffect, useState} from 'react';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import {setProducts, setProductsCategory} from '../redux/slice/user';
import store from '../redux/store';
import { useNavigation } from '@react-navigation/native';



const height = Dimensions.get('window').height;
const width =Dimensions.get('window').width;

const TestScreen = () => {
  const navigations = useNavigation();
  const dispatch = useDispatch();
  const products = useSelector(state=> state.user.products);
  const category = useSelector(state => state.user.productsCategory);
  console.log('products is', products)

  const[goods,setGoods] =useState([]);
  const [image,setImage] =useState();
  const[jewel,setJewel] =useState();
  const[price,setPrice] =useState();
  const[display,setDisplay] = useState(true)
  const[category1,setCategory1] =useState()
  const[newData,setNewData] = useState(newData)
  const[loader,setLoader] =useState(true)


   console.log('newdata', newData)
 const SearchProducts= (searchInput)=>{
  setDisplay(false);
  const searchData = products.filter((item)=>{
    console.log('searchdata', searchData);
    return item.category.toLowerCase().includes(searchInput.toLowerCase())
    
  });
 setNewData(searchData);



     
    

}

//  if(item.category.toUpperCase().includes(input2.toUpperCase())){
//   setGoods(()=>([...goods,item.category]));
//   console.log('searc products ', item.category,item.id);
//   return(
//     <ItemCard
//       test={item.title}
//       photo={item.image}
//       category={item.category}
//       price={item.price}
//     /> 
//   )

  

// const searchProducts =  async ()=>{
//   if(goods=="jewelery"){
//     setDisplay(false);
//     await  axios 
//     .get( jewelery1)
//     .then(response=>{
//       if(response.data){
//         setJewel(response.data)
 
//       }
//       else{
//         alert(Response.data.message)
//       }
//     })
  
//     .catch(err =>{
//       console.log(err);
//     });

 
// }
// else {
//   setDisplay(true)
//   console.log('nodata')
// }
// }

  return (
   
     <View >
      <View style={styles.main}>
      <View style={styles.search}>
        <View style={styles.options}>
          <TouchableOpacity>
            <Menu name="menu" size={25} color={colours.Black} />
          </TouchableOpacity>
        </View>

        <View style={styles.searchBar}>
        <TouchableOpacity style={styles.searchButton} ><Search name="search1" style={styles.searchIcon} size={22} /></TouchableOpacity>
          <TextInput style={styles.searchText} placeholder="search here..." onChangeText={(value)=>SearchProducts(value)}></TextInput>
        </View>
      </View>
      
    

      <View style={styles.header}>
        <Text style={styles.headerText}>Order online collect in store</Text>
      </View>


      {display ? 
      <FlatList
        key="&"
        data={products}
        renderItem={({item}) => (
          
          <ItemCard
            test="Please Search items"
            photo={item.image}
            category={item.category}
            price={item.price}
          /> 
        )}
        keyExtractor={item => '&' + item.id}
       
      />
      
        : 
      
      <FlatList
        key="search"
        data={newData}
        renderItem={({item}) => (
          
          <ItemCard
            test={item.title}
            photo={item.image}
            category={item.category}
            price={item.price}
          /> 
        )}
        keyExtractor={item => 'search' + item.id}
       
      />
      }


</View> 
    </View>
  
  )
}

export default TestScreen;

const styles = StyleSheet.create({
  main: {
    height: height,
    width: width,
  },
  search: {
    height: height * 0.1,
    width: width * 0.9,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  options: {
    height: height * 0.05,
    width: width * 0.1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchBar: {
    height: height * 0.05,
    width: width * 0.7,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: colours.borderDark,
    flexDirection:'row',
    alignItems: 'center',
    
  },

  searchText: {
    height: height * 0.05,
    width: width * 0.6,
    borderRadius: 30,
    
  },
  filter:{
    height: height * 0.05,
    width: width * 0.7,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: colours.borderDark,
    paddingLeft: 10,
  },
  header: {
    height: height * 0.2,
    width: width * 0.9,
    alignSelf: 'center',
    marginTop: 10,
  },
  headerText: {
    color: colours.Black,
    fontSize: 43,
    fontWeight: '700',
  },
  searchButton:{
    width:width*0.1,
    height:height*0.05,
    alignItems:'center',
    justifyContent:'center',
  }
});