import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colours} from '../constants';
import {useNavigation} from '@react-navigation/native';
import SplashScreen from './SplashScreen';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const WelcomeScreen = () => {
  const navigation = useNavigation();
  const [screen, setScreen] = useState(false);

  useEffect(() => {
    const splash = setTimeout(() => {
      setScreen(true);
    }, 1500);
    return () => {
      clearTimeout(splash);
    };
  }, []);

  return (
    <View>
      {screen ? (
        <View style={styles.main}>
          <View style={styles.header}>
            <Text style={styles.headerText}>Find your Gadget</Text>
          </View>
          <View style={styles.splashPhoto}>
            <Image
              source={require('../assets/images/splashImage(2).png')}
              style={styles.splashImage}
            />
          </View>
          <View>
            <TouchableOpacity
              style={styles.splashTouch}
              onPress={() => {
                navigation.navigate('LoginScreen');
              }}>
              <Text style={styles.buttonText}>Get started</Text>
            </TouchableOpacity>
          </View>
        </View>
      ) : (
        <SplashScreen />
      )}
    </View>
  );
};

export default WelcomeScreen;

const styles = StyleSheet.create({
  main: {
    height: height,
    width: width,
    backgroundColor: colours.Blue,
    alignItems: 'center',
  },
  header: {
    height: height * 0.18,
    width: width * 0.75,
    alignSelf: 'center',
    marginTop: 20,
  },
  headerText: {
    color: colours.white,
    fontSize: 50,
    fontWeight: '800',
  },
  splashPhoto: {
    height: height * 0.6,
    width: width * 0.85,
    alignSelf: 'center',
  },
  splashImage: {
    width: 400,
    height: 500,
    alignSelf: 'center',
  },
  splashTouch: {
    height: 60,
    width: width * 0.7,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colours.white,
    marginTop: 30,
  },
  buttonText: {
    fontSize: 20,
    color: colours.Blue,
    fontWeight: '400',
  },
  emptyView: {
    height: 50,
    width: width,
    backgroundColor: colours.blurBlue,
  },
});
